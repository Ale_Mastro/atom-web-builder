import React from 'react';
import {useDraggable} from '@dnd-kit/core';

export default function Draggable(props) {
  const {attributes, listeners, setNodeRef, transform, isDragging} = useDraggable({
    id: props.id,
  });
  const style = transform ? {
    transform: `translate3d(${transform.x}px, ${transform.y}px, 0)`,
  } : undefined;

  
  return (
    <button ref={setNodeRef} style={style} {...listeners} {...attributes} 
    onMouseOver={() => {isDragging ? props.selected() : null}}>
      {props.children}
    </button>
  );
}