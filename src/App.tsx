import React, {useState} from 'react';
import {DndContext} from '@dnd-kit/core';
import {MultipleContainers} from '@dnd-kit/sortable'

import Droppable from './Droppable';
import Draggable from './Draggable';

function App() {
  const [isDropped, setIsDropped] = useState('');
  const [isGrabbed, setIsGrabbed] = useState('');
  const draggableMarkup = (id) => (
    <Draggable id={id} selected={() => setIsGrabbed(id)}>Drag me</Draggable>
  );
  
  return (
    <div className='w-[100vw] h-[100vh] bg-white'>
    <DndContext onDragEnd={handleDragEnd}>
      <div className='w-full h-full flex'>
        <div className='w-1/2'>
      {isDropped !== 'id1' ? draggableMarkup('id1') : null}
      {isDropped !== 'id2' ? draggableMarkup('id2') : null}
      {isDropped !== 'id3' ? draggableMarkup('id3') : null}

        </div>
        <div className='w-1/2'>
        <Droppable>
        {isDropped ? draggableMarkup(isGrabbed) : 'Drop here'}
      </Droppable>
      <MultipleContainers 
    containerStyle={{
      maxHeight: '80vh',
    }}
    itemCount={15}
    scrollable
  />
        </div>
      </div>
    </DndContext> 
    </div>
  );
  
  function handleDragEnd(event) {
    if (event.over && event.over.id === 'droppable') {
      // console.log(event.over)
      setIsDropped(isGrabbed);
    }
  }
}

export default App;